class SemanticResult {
    private boolean flag;
    private GlobalEnv env;

    public SemanticResult(boolean flag, GlobalEnv env){
	this.env = env;
	this.flag = flag;
    }
    public boolean getFlag(){
	return this.flag;
    }
    public GlobalEnv getEnv(){
	return this.env;
    }
}
