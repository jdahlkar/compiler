import java.util.*;

class MipsGen {
    static String program = "\n.text\n";
    static List<Integer> formals;
    static int frame;
    static String labend = "";
    static String data = ".data\n";
    static Map<String, Integer> frames = new HashMap<String, Integer>();
    static void mgen(List<RtlDec> rtlProgram) {
	for(RtlDec dec : rtlProgram) {
	    mgenDec(dec);
	}
	System.out.print(data + program + syscalls);
    }

    static void mgenDec(RtlDec dec){
	if(dec instanceof Proc) {
	    program += "# Function start:\n";
	    String label = ((Proc)dec).getLabel();
	    if(!label.equals("putstring") &&
	       !label.equals("getstring") &&
	       !label.equals("putint") &&
	       !label.equals("getint")) {
		formals = ((Proc)dec).getFormals();
		frame = ((Proc)dec).getFrame();
		int nL = ((Proc)dec).getLoc();
		int nF = formals.size();
		frames.put(label, frame);
		program += label + ":\n";
		labend = "end_"+ label;
		
		// Save old fp
		program += "sw $fp, "+ (4*nF+12+frame) +"($sp)\n";
		//program += "addiu $sp, $sp, -4\n";

		if(!label.equals("main")) {
		    // Back up frame pointer
		    program += "addiu $fp, $sp, " + (4*nF+8+frame)+ "\n";
		} else {
		    program += "addiu $fp, $sp, 0\n";
		    program += "addiu $sp, $sp, "+ -(frame+8) +"\n";
		}

		// Set SP to top of stack
		program += "addiu $sp, $sp, " + -(4*nL) + "\n";
		// Save current fp for proper rtl translation
		program += "sw $fp, "+ -(frame+4) +"($fp)\n";
		// Save old ra on stack
		program += "sw $ra, " + -(frame) + "($fp)\n";

		// Generate subr code
		for(RtlInsn ins : ((Proc)dec).getInsns()) {
		    mgenInsn(ins);
		}
		// Label for returns/recovery from subr
		program += labend + ":\n";
		// Reset ra to old ra
		program += "lw $ra, " + -(frame) + "($fp)\n";
		// Reset sp to old top of stack
		program += "addiu $sp, $fp, 4\n";
		// Load old fp
		program += "lw $fp, 4($fp)\n";
		// Return to code before call
		program += "jr $ra\n";
		program += "# Function end\n";
	    } 
	} else {
	    // Keep data aligned to multiples of 4 by sizes divisable by 4
	    int size = ((Data)dec).getSize()%4==0 ?
		((Data)dec).getSize() :
		(((Data)dec).getSize()/4+1)*4;
	    data += "G" +((Data)dec).getLabel() + ": .space " + 
		size + "\n";
	}
    }

    static void mgenCall(Call c) {
	String label =  c.getLabel();
	int offs = 0;
	int temp = c.getTemp();
	if(!label.equals("putstring") &&
	   !label.equals("getstring") &&
	   !label.equals("putint") &&
	   !label.equals("getint")) {
	    offs = frames.get(label);

	    // Alloc space for old fp, frame, old ra and curr fp space on stack 
	    program += "addiu $sp, $sp, " + (-(offs+12)) + "\n";
	}
	for(int arg : c.getArgs()) {
	    program += "lw $t0, " +  (-(4*arg+frame)) + "($fp)\n";
	    program += "sw $t0, 0($sp)\n";
	    program += "addiu $sp $sp -4\n";
	}
	program += "jal " + label + "\n";
	program += "sw $v0, " + -(4*temp+frame)+ "($fp)\n";
	program += "addiu $sp, $sp, -4\n";
    }

    static void mgenInsn(RtlInsn ins) {
	if(ins instanceof Store){
	    program += "# Store start:\n";
	    if(!((Store)ins).isArr()){
		program += "lw $t0, " + -(4*((Store)ins).getVal()+frame) + "($fp)\n";
		program += ((Store)ins).getType() == RtlType.LONG ?
		    "sw $t0, " + -(4*((Store)ins).getAddr()+frame) + "($fp)\n" :
		    "sb $t0, " + -(4*((Store)ins).getAddr()+frame) + "($fp)\n";
	    }else{
		program += "lw $t0, " + -(4*((Store)ins).getVal()+frame) + "($fp)\n";
		program += "lw $t1, " + -(4*((Store)ins).getAddr()+frame) + "($fp)\n";
		program += ((Store)ins).getType() == RtlType.LONG ?
		    "sw $t0, 0($t1)\n" : "sb $t0, 0($t1)\n";
	    }
	    program += "addiu $sp $sp -4\n";
	    program += "# Store end\n";
		
	} else if (ins instanceof Load) {
	    program += "# Load start:\n";
	    if(((Load)ins).isGlobal()) {
	    program += ((Load)ins).getType() == RtlType.LONG ?
		"lw $t0, " + -(4*((Load)ins).getAddr()+frame) + "($fp)\n" :
		"lb $t0, " + -(4*((Load)ins).getAddr()+frame) + "($fp)\n";
	    } else {
		program += "lw $t0, " + -(4*((Load)ins).getAddr()+frame) + "($fp)\n";
		program += ((Load)ins).getType() == RtlType.LONG ?
		    "lw $t0, 0($t0)\n" :
		    "lb $t0, 0($t0)\n";
		program += "sw $t0, "+ -(4*((Load)ins).getAddr()+frame+4) +"($fp)\n";
		program += "addiu $sp, $sp, -4\n";
	    }
	    program += "# Load end\n";
	} else if (ins instanceof Eval) {
	    program += "# Eval start:\n";
	    int temp = ((Eval)ins).getTemp();
	    RtlExp exp = ((Eval)ins).getExp();
	    mgenExp(temp, exp);
	    program += "# Eval end\n";
	} else if (ins instanceof LabDef) {
	    program += ((LabDef)ins).getLabel() + ":\n";
	} else if (ins instanceof CJump){
	    program += "# CJump start:\n";
	    mgenCJ((CJump)ins);
	    program += "# CJump end\n";
	} else if (ins instanceof Jump) {
	    program += "j " + ((Jump)ins).getLabel() + "\n";
	} else if (ins instanceof Call) {
	    program += "# Call start:\n";
	    mgenCall((Call)ins);
	    program += "# Call end\n";
	}
    }

    static void mgenCJ(CJump cj) {
	int cond = cj.getTemp();
	program += "lw $t0, " + -(4*cond+frame) + "($fp)\n";
	program += "beq $t0, $zero, " + cj.getLabel() + "\n";
    }
    static void mgenExp(int temp, RtlExp exp) {
	if(exp instanceof TempExp) {
	    program += "# TempExp Start\n";
	    int val = ((TempExp)exp).getTemp();
	    program += "lw $v0, " + -(4*val+frame) + "($fp)\n";///// -----------
	    program += "j " + labend + "\n";
	    program += "# TempExp End\n";
	} else if(exp instanceof Binary) {
	    program += "# Binary Start\n";
	    mgenBinary(temp, (Binary)exp);
	    program += "# Binary End\n";
	} else if(exp instanceof Icon) {
	    program += "# Icon Start\n";
	    program += "li $t0, " + ((Icon)exp).getVal() + "\n";
	    program += "sw $t0 " + -(temp*4+frame) + "($fp)\n";
	    program += "addiu $sp $sp -4\n";
	    program += "# Icon End\n";
	} else {
	    program += "# Labref Start\n";
	    program += "la $t0, " + ((LabRef)exp).getLabel() +"\n";
	    program += "sw $t0 " + -(temp*4+frame) + "($fp)\n";
	    program += "addiu $sp $sp -4\n";
	    program += "# Labref End\n";
	}
    }
    static void mgenBinary(int temp, Binary exp) {
	int left = exp.getLeft();
	program += "lw $t0, " + (left == 1 ? 0 : -(4*left+frame)) + "($fp)\n";
	
	int right = exp.getRight();
	program += "lw $t1, " +  -(4*right+frame) + "($fp)\n";
	switch(exp.getOp()) {
	case EQ:
	    program += "xor $t2, $t0, $t1\n";
	    program += "sltiu $t2, $t2, 1\n";
	    break;
	case NE:
	    program += "xor $t2, $t0, $t1\n";
	    break; 
	case AND:
	    program += "slt $t0, $zero, $t0\n";
	    program += "slt $t1, $zero, $t1\n";
	    program += "add $t0, $t0, $t1\n";
	    program += "li $t3, 1\n";
	    program += "slt $t2, $t3, $t0\n";
	    break;
	case LT:
	    program += "slt $t2, $t1, $t0\n";
	    break;
	case GT: 
	    program += "slt $t2, $t0, $t1\n";
	    break;
	case LTEQ: 
	    program += "addi $t0, $t0, 1\n"; ////------------
	    program += "slt $t2, $t1, $t0\n"; //// -----------
	    break;
	case GTEQ: 
	    program += "addi $t0, $t0, -1\n";
	    program += "slt $t2, $t0, $t1\n";
	    break;
	case PLUS:
	    program += "add $t2, $t0, $t1\n";
	    break;
	case MINUS:
	    program += "sub $t2, $t1, $t0\n";
	    break;
	case MUL: 
	    program += "mul $t2, $t0, $t1\n";
	    break;
	case DIV:
	    program += "div $t2, $t1, $t0\n";
	    //program += "ori $t2, $lo, 0\n";
	    break;
	default:
	}
	program += "sw $t2 " + -(temp*4+frame) + "($fp)\n";
	program += "addiu $sp $sp -4\n";
    }

    static String syscalls = "\n.data\n"+
	".globl putint \n"+
	".globl putstring\n"+
	".globl getint\n"+
	".globl getstring\n\n"+
	".text\n"+
	"\nputint:\n"+
	"addi      $sp, $sp, -24\n"+
	"sw        $fp, 4($sp)\n"+
	"sw        $ra, 8($sp)\n"+
	"addi      $fp, $sp, 24\n"+
	"lw        $a0, 4($fp)\n"+
	"li 	  $v0, 1\n"+
	"syscall\n"+
	"lw        $fp, 4($sp)\n"+	
	"lw        $ra, 8($sp)\n"+
	"addi      $sp, $sp, 24\n"+
	"jr        $ra\n"+
	"\nputstring:\n"+
	"addi      $sp, $sp, -24\n"+
	"sw        $fp, 4($sp)\n"+
	"sw        $ra, 8($sp)\n"+
	"addi      $fp, $sp, 24\n"+
	"lw        $a0, 4($fp)\n"+
	"li 	  $v0, 4\n"+
	"syscall\n"+
	"lw        $fp, 4($sp)	\n"+
	"lw        $ra, 8($sp)\n"+
	"addi      $sp, $sp, 24\n"+
	"jr        $ra\n"+
	"\ngetint:\n"+
	"addi      $sp, $sp, -24\n"+
	"sw        $fp, 4($sp)\n"+
	"sw        $ra, 8($sp)\n"+
	"addi      $fp, $sp, 24\n"+
	"li 	  $v0, 5\n"+
	"syscall\n"+
	"lw        $fp, 4($sp)	\n"+
        "lw        $ra, 8($sp)\n"+
        "addi      $sp, $sp, 24\n"+
        "jr        $ra\n"+
	"\ngetstring:\n"+
        "addi      $sp, $sp, -24\n"+
        "sw        $fp, 4($sp)\n"+
        "sw        $ra, 8($sp)\n"+
        "addi      $fp, $sp, 24\n"+
	"lw        $a0, 4($fp)\n"+
	"li        $a1, 80\n"+
	"li 	  $v0, 8\n"+
	"syscall\n"+
	"lw        $fp, 4($sp)	\n"+
        "lw        $ra, 8($sp)\n"+
        "addi      $sp, $sp, 24\n"+
        "jr        $ra\n";
}
