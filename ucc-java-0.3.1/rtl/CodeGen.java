import java.util.*;

class CodeGen {
    static int cnt;
    static int labCnt;
    static int frame;
    static int numberOfLocs;
    static Map<String, RtlDec> rtlProgram = new HashMap<String, RtlDec>();
    static List<RtlDec> rtlCode = new ArrayList<RtlDec>();
    static List<Integer> form = new ArrayList<Integer>();

    static List<RtlDec> cgen(List<Node> program, GlobalEnv env){
	
	for(Node td : program){
	    cgenTd(td, env);
	}
	for(RtlDec ins : rtlCode){
	    //System.out.println(ins.toString());
	}
	System.out.println("\n# END OF RTL"); 
	return rtlCode;
    }
    
    static void cgenTd(Node td, GlobalEnv env){
	switch(td.getId()) {
	case FUNC:
	case EXTERN:
	    cgenProc(td, env);
	    break;
	case VARDEC:
	    cgenData(td, env);
	    break;
	default:
	    
	}
    }

    static void cgenData(Node varDec, GlobalEnv env) {
	Node bt = varDec.getChild(0,2);
	Node dr = varDec.getChild(1,2);
	Type t;
	RtlDec dec = null;
	int size;
	String label = ((IdentifierNode)dr.getChild(0)).getName();
	switch(dr.getId()){
	case SCALAR_TYPE:
	    switch(bt.getId()){
	    case INT:
		dec = new Data(label,
			       Rtl.sizeOf(RtlType.LONG));
		break;
	    case CHAR:
		dec = new Data(label,
			       Rtl.sizeOf(RtlType.BYTE));
		break;
	    default:
		
		break;
	    }
	    break;
	case ARR_TYPE:
	    size = ((IntegerLiteralNode)dr.getChild(1,2)).getValue();
	    switch(bt.getId()){
	    case INT:
		dec = new Data(label,
			       Rtl.sizeOf(RtlType.LONG) * size);
		break;
	    case CHAR:
		dec = new Data(label,
			       Rtl.sizeOf(RtlType.BYTE) * size);
		break;
	    default:
		
		break;
	    }
	    break;
	default:
	    break;
	}
	rtlProgram.put("G"+label, dec);
	rtlCode.add(dec);
    }

    static void cgenProc(Node f, GlobalEnv env) {
	frame = 0;
	CompoundStatementNode b;
	String label;
	LocalEnv lenv = null;
	List<Node> formals;
	frame = 0;
	if(f.getId() == Id.FUNC){
	    b =(CompoundStatementNode)((FuncNode)f).getBody();
	    label = ((IdentifierNode)(((FuncNode)f).getIdent().getChild(0))).getName();
	    formals =((FuncNode) f).getFormals();
	    lenv = env.getEnv(label);
	} else {
	    f = (ExternNode)f;
	    formals = ((ExternNode)f).getFormals();
	    label = ((IdentifierNode)(((ExternNode)f).getIdent().getChild(0))).getName();
	    b = null;
	    lenv = env.getEnv("Ex" + label);
	}
	Proc func = (Proc)rtlProgram.get(label);
	List<Integer> loc = new ArrayList<Integer>();
	form = new ArrayList<Integer>();
	List<RtlInsn> insn = new ArrayList<RtlInsn>();
	cnt = 2;
	if(func != null){
	    cnt = formals.size()+2;
	    func.setInsns(mkBody(b, lenv, insn, loc));
	    func.setFrame(frame);
	    func.setLocals(loc);
	    return;
	}
	for(Node forml : formals) {
	    form.add(cnt);
	    Node c = forml.getChild(1,2);
	    String name = ((IdentifierNode)c.getChild(0)).getName();
	    Type t = lenv.lookup(name);
	    t.setReg(cnt);
	    cnt++; 
	}
	List<RtlInsn> bodyInsn = mkBody(b, lenv, insn, loc);
        RtlDec proc = new Proc(label, form, loc, frame, 
			     bodyInsn);
	((Proc)proc).setLoc(numberOfLocs);
	rtlProgram.put(label, proc);
	rtlCode.add(proc);
    }

    static int foffs(int frame) {
	return frame %4 == 0 ? frame : (frame/4+1)*4;
    }

    static List<RtlInsn> mkBody(CompoundStatementNode body, LocalEnv env,
		       List<RtlInsn> insn, List<Integer> loc){
	numberOfLocs = 0;
	if(body == null) return null;
	List<Node> nloc = body.getDeclarations();
	for(Node dec : nloc) {
	    Id ctype = dec.getChild(0,2).getId();
	    Node c = dec.getChild(1,2);
	    if(c.getId() == Id.ARR_TYPE) {
		IntegerLiteralNode il = (IntegerLiteralNode)c.getChild(1,2);
		int size = il.getValue();
		IdentifierNode id = (IdentifierNode)c.getChild(0,2);
		String name = id.getName();
		Type t = env.lookup(name);
		t.setFrameOff(frame);
		frame += size * (ctype == Id.CHAR ?
				 Rtl.sizeOf(RtlType.BYTE) :
				 Rtl.sizeOf(RtlType.LONG));
		frame = foffs(frame);
	    } else {
		loc.add(cnt);
		String name = ((IdentifierNode)c.getChild(0)).getName();
		Type t = env.lookup(name);
		t.setReg(cnt);
		numberOfLocs++;
		cnt++; 
	    }
	}
	
	List<Node> stmt = body.getStatements();
	for(Node s : stmt) {
	    mkRtlInsn(s, env, insn, loc);
	}
	//insn.add(new LabDef("ret0"));
	return insn;
    }

    static void mkRtlInsn(Node stm, LocalEnv env, 
			     List<RtlInsn> insn, List<Integer> loc){
	int cond;
	Eval e;
	switch(stm.getId()){
	case SIMPLE_COMPOUND_STMNT:
	    List<Node> stms = ((SimpleCompoundStatementNode)stm).getStatements();
	    for(Node s : stms){
		mkRtlInsn(s, env, insn, loc);
	    }
	    break;
	case STMNT:
	    int exp = mkRtlExp(stm.getChild(0), env, insn, loc);
	    break;
	case EMPTY_STMNT:
	    break;
	case IF:
	    int n = stm.numberOfChildren();
	    Node b = stm.getChild(0,n);
	    Node s1 = stm.getChild(1,n);
	    String _el = "el" + labCnt;
	    String end = "end" + labCnt;
	    cond = mkRtlExp(b, env, insn, loc);
	    labCnt++;
	    if(n == 3){
		// cjump true/false l2
		CJump cj = new CJump(false, cond, _el);
		insn.add(cj);
		// if stuff
		mkRtlInsn(s1, env, insn, loc);
		// jump l2
		Jump j = new Jump(end);
		insn.add(j);
		// l1: 
		LabDef l = new LabDef(_el);
		insn.add(l);
		// else stuff
		mkRtlInsn(stm.getChild(2,n), env, insn, loc);
		// l2:
		l = new LabDef(end);
		insn.add(l);
		// rest of code
	    } else {
		// cjump true/false l1
		CJump cj = new CJump(false, cond, end);
		insn.add(cj);
		// if stuff
		mkRtlInsn(s1, env, insn, loc);
		// l1:
		LabDef l = new LabDef(end);
		insn.add(l);
		//rest of code

	    }
	    break;
	case WHILE:
	    String loop = "loop" + labCnt;
	    String stop = "stop" + labCnt;
	    labCnt++;
	    // l1:
	    LabDef l = new LabDef(loop);
	    insn.add(l);
	    cond = mkRtlExp(stm.getChild(0,2), env, insn, loc);
	    // cjump true/false l2
	    CJump cj = new CJump(false, cond, stop);
	    insn.add(cj);
	    // while stuff
	    mkRtlInsn(stm.getChild(1,2), env, insn, loc);
	    // jump l1
	    Jump j = new Jump(loop);
	    insn.add(j);
	    // l2:
	    l = new LabDef(stop);
	    insn.add(l);
	    // rest of code
	    break;
	case RETURN:
	    int z = stm.numberOfChildren();
	    if(z==1){
		int ret = mkRtlExp(stm.getChild(0), env, insn, loc);
		e = new Eval(0, new TempExp(ret));
		insn.add(e);
	    }
	default:
	    
	}
    }
    
    static int mkRtlExp(Node exp, LocalEnv env, 
			 List<RtlInsn> insn, List<Integer> loc){
	Eval e;
	switch(exp.getId()){
	case BINARY:
	    return mkRtlBinary((BinaryNode)exp, env, insn, loc);
	case UNARY:
	    return mkRtlUnary((UnaryNode)exp, env, insn, loc);
	case FCALL:
	    List<Integer> forms = new ArrayList<Integer>();
	    List<Node> args = ((FCallNode)exp).getArgs();
	    IdentifierNode id = (IdentifierNode)((FCallNode)exp).getIdent();
	    String label = id.getName();
	    for(Node a : args){
		int r = mkRtlExp(a, env, insn, loc);
		forms.add(r);
	    }
	    Call fCall = new Call(cnt, label, forms);
	    insn.add(fCall);
	    loc.add(cnt);
	    return (++cnt - 1);
	case ASSIGN:
	    return mkRtlAssign(exp, env, insn, loc);
	case CHAR_LITERAL:
	    String v = ((CharLiteralNode)exp).getValue();
	    int ascii = (int)v.charAt(1);
	    if(ascii == '\\'){
		ascii = 10;
	    }
	    RtlExp c = new Icon(ascii);
	    e = new Eval(cnt, c);
	    insn.add(e);
	    loc.add(cnt);
	    return (++cnt - 1);
	case INTEGER_LITERAL:
	    RtlExp i = new Icon(((IntegerLiteralNode)exp).getValue());
	    e = new Eval(cnt, i);
	    insn.add(e);
	    loc.add(cnt);
	    return (++cnt - 1);
	case IDENT:
	    String name = ((IdentifierNode)exp).getName();
	    Type t = env.lookup(name);
	    if(rtlProgram.get("G"+name) != null  && !t.isLocal()) {
		e = new Eval(cnt, new LabRef("G"+name));
		insn.add(e);
		loc.add(cnt);
		cnt++;
		if(t.getId() != Id.INT && t.getId() != Id.CHAR){
		    
		    return cnt-1;
		}
		RtlType rt = t.getId() == Id.INT ? 
		    RtlType.LONG :
		    RtlType.BYTE;
		Load l = new Load(rt, cnt-1, cnt);
		l.setIsGlobal(false);
		insn.add(l);
		loc.add(cnt);
		return (++cnt - 1);
	    }
	    if((t.getId() == Id.ARR_CHAR ||
		t.getId() == Id.ARR_INT) && !form.contains(t.getReg())){
		int offs = t.getFrameOff();
		e = new Eval(cnt, new Icon(offs));
		insn.add(e);
		loc.add(cnt);
		cnt++;
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, Rtl.FP, cnt-1));
		insn.add(e);
		loc.add(cnt);
		cnt++;
		return cnt-1;
		//return 2;
	    }
	    return t.getReg();
	case ARRAY:
	    IdentifierNode var = (IdentifierNode)exp.getChild(0,2);
	    int index = mkRtlExp(exp.getChild(1,2), env, insn, loc);
	    t = env.lookup(var.getName());
	    RtlType rt;
	    int size = t.getId() == Id.ARR_CHAR ?
		Rtl.sizeOf(rt = RtlType.BYTE) : 
		Rtl.sizeOf(rt = RtlType.LONG);
	    e = new Eval(cnt, new Icon(size));
	    insn.add(e);
	    loc.add(cnt);
	    e = new Eval(cnt+1, new Binary(RtlBinop.MUL, cnt, index));
	    cnt++;
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    if(rtlProgram.get("G"+var.getName()) != null && !t.isLocal()) {
		e = new Eval(cnt, new LabRef("G"+var.getName()));
	    }else if(!form.contains(t.getReg())) {
		int offs = t.getFrameOff();
		e = new Eval(cnt, new Icon(offs));
	    }
	    if(!form.contains(t.getReg())) {
		insn.add(e);
		loc.add(cnt);
		cnt++;
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, cnt-1, cnt-2));
	    } else {
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, t.getReg(), cnt-1));
	    }
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    if(t.isLocal() && !form.contains(t.getReg())) {
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, Rtl.FP, cnt-1));
		insn.add(e);
		loc.add(cnt);
		cnt++;
	    }
	    Load l = new Load(rt, cnt-1, cnt);
	    //if(!form.contains(t.getReg())) {
	    l.setIsGlobal(false);
	    //}
	    insn.add(l);
	    loc.add(cnt);
	    cnt++;
	    return (cnt-1);
	default:
	    
	}
	return -1;
    }
    static int mkRtlAssign(Node a, LocalEnv env,
			   List<RtlInsn> insn, List<Integer> loc){
	Node var = a.getChild(0,2);
	IdentifierNode var2;
	Eval e;
	Type expType, t;

	int reg = mkRtlExp(a.getChild(1,2), env, insn, loc);
	if(var.getId() == Id.ARRAY){
	    var2 = (IdentifierNode)var.getChild(0,2);
	    t = env.lookup(var2.getName());
	    int index = mkRtlExp(var.getChild(1,2), env, insn, loc);
	    RtlType rt;
	    int size = t.getId() == Id.ARR_CHAR ?
		Rtl.sizeOf(rt = RtlType.BYTE) : 
		Rtl.sizeOf(rt = RtlType.LONG);
	    e = new Eval(cnt, new Icon(size));
	    insn.add(e);
	    loc.add(cnt);
	    e = new Eval(cnt+1, new Binary(RtlBinop.MUL, cnt, index));
	    cnt++;
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    if(rtlProgram.get("G"+var2.getName()) != null && !t.isLocal()) {
		e = new Eval(cnt, new LabRef("G"+var2.getName()));
	    }else if(!form.contains(t.getReg())) {
		int offs = t.getFrameOff();
		e = new Eval(cnt, new Icon(offs));
	    }
	    if(!form.contains(t.getReg())) {
		insn.add(e);
		loc.add(cnt);
		cnt++;
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, cnt-1, cnt-2));
	    } else {
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, t.getReg(), cnt-1));
	    }
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    if(t.isLocal() && !form.contains(t.getReg())) {
		e = new Eval(cnt, new Binary(RtlBinop.PLUS, Rtl.FP, cnt-1));
		insn.add(e);
		loc.add(cnt);
		cnt++;
	    }
	    Store s = new Store(rt, cnt-1, reg);

	    insn.add(s);
    	    return -1;
	} else {
	    var2 = (IdentifierNode)var;
	    t = env.lookup(var2.getName());
	    if(rtlProgram.get("G"+var2.getName()) != null  && !t.isLocal()) {
		e = new Eval(cnt, new LabRef("G"+var2.getName()));
		insn.add(e);
		loc.add(cnt);
		//cnt++;
		RtlType rt = t.getId() == Id.INT ? 
		    RtlType.LONG :
		    RtlType.BYTE;
		Store s = new Store(rt, cnt, reg);
		//s.setArr(false);

		insn.add(s);
		return (++cnt - 1);
	    }
	    
	    //t.setReg(reg);
	    RtlType rt = t.getId() == Id.INT ? 
		RtlType.LONG :
		RtlType.BYTE;
	    Store s = new Store(rt, t.getReg(), reg);
	    s.setArr(false);
	    insn.add(s);
	    return t.getReg();
	}
    }
    static int mkRtlUnary(UnaryNode b, LocalEnv env,
			  List<RtlInsn> insn, List<Integer> loc){
	Eval e;
	int exp = mkRtlExp(b.getChild(0), env, insn, loc);
	switch(b.getOp()) {
	case NOT:
	    e = new Eval(cnt, new Icon(0));
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    e = new Eval(cnt, new Binary(RtlBinop.EQ, cnt-1, exp));
	    insn.add(e);
	    loc.add(cnt);
	    return (++cnt - 1);
	case NEG:
	    //RtlExp exp = new BinaryNode(RtlBinop.MINUS,
	    e = new Eval(cnt, new Icon(0));
	    insn.add(e);
	    loc.add(cnt);
	    cnt++;
	    e = new Eval(cnt, new Binary(RtlBinop.MINUS, cnt-1, exp));
	    insn.add(e);
	    loc.add(cnt);
	    return (++cnt - 1);
	default:
	    
	}
	return -1;
    }

    static int mkRtlBinary(BinaryNode b, LocalEnv env,
			    List<RtlInsn> insn, List<Integer> loc){
	int l = mkRtlExp(b.getChild(0,2), env, insn, loc);//left
	int r = mkRtlExp(b.getChild(1,2), env, insn, loc);//right
	RtlExp exp = new Icon(-1);
	switch(b.getOp()){
	case ANDAND:
	    exp = new Binary(RtlBinop.AND, l, r);
	    break;
	case EQEQ:
	    exp = new Binary(RtlBinop.EQ, l, r);
	    break;
	case NE:
	    exp = new Binary(RtlBinop.NE, l, r);
	    break;
	case LT:
	    exp = new Binary(RtlBinop.LT, l, r);
	    break;
	case GT:
	    exp = new Binary(RtlBinop.GT, l, r);
	    break;
	case LTEQ:
	    exp = new Binary(RtlBinop.LTEQ, l, r);
	    break;
	case GTEQ:
	    exp = new Binary(RtlBinop.GTEQ, l, r);
	    break;
	case PLUS:
	    exp = new Binary(RtlBinop.PLUS, l, r);
	    break;
	case MUL:
	    exp = new Binary(RtlBinop.MUL, l, r);
	    break;
	case DIV:
	    exp = new Binary(RtlBinop.DIV, l, r);
	    break;
	case MINUS:
	    exp = new Binary(RtlBinop.MINUS, l, r);
	    break;
	default:
	    System.out.println("lofo");
	    break;
	}
	Eval e = new Eval(cnt, exp);
	insn.add(e);
	loc.add(cnt);
	return (++cnt -1);
    }
}
