/* Generated By:JavaCC: Do not edit this line. UcParse.java */
import java.util.*;
import java.io.*;

class UcParse implements UcParseConstants {
  public static void main(String[] arg)
  throws ParseException, IOException {
    if (arg.length != 1) {
        System.out.println("Usage: UcParse <input file name>");
        System.exit(0);
    }
    InputStream is = new FileInputStream(arg[0]);

    UcParse parser = new UcParse(is);
    List<Node> program = Start();
    SemanticResult result = Semantic.checkProgram(program);
    boolean flag = result.getFlag();
    if (flag){
        //System.out.println("Check succeeded");
        List<RtlDec> rtlProgram = CodeGen.cgen(program, result.getEnv());
        MipsGen.mgen(rtlProgram);
    }
    if (!flag){
        System.out.println("Check failed");
    }
  }

  static final public List<Node> Start() throws ParseException {
    Node td, temp;
    List<Node> tds = new ArrayList<Node>();
    label_1:
    while (true) {
      td = TopLevelDeclaration();
          tds.add(td);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case CHAR:
      case INT:
      case VOID:
        ;
        break;
      default:
        jj_la1[0] = jj_gen;
        break label_1;
      }
    }
         {if (true) return tds;}
    throw new Error("Missing return statement in function");
  }

  static final public Node TopLevelDeclaration() throws ParseException {
    Node  body, dr, bt, temp;
    List<Node> formals;
    bt = BaseType();
    dr = Declarator();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case LPAREN:
      formals = FunctionParameters();
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LBRACE:
        body = CompoundStatement();
         temp = new FuncNode(dr, bt, formals, body);
         temp.setPosition(bt.getPosition());
         {if (true) return temp;}
        break;
      case SEMI:
        jj_consume_token(SEMI);
         temp = new ExternNode(dr, bt, formals);
         temp.setPosition(bt.getPosition());
         {if (true) return temp;}
        break;
      default:
        jj_la1[1] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    case SEMI:
      jj_consume_token(SEMI);
        temp = new Node(Id.VARDEC);
        temp.add(bt);
        temp.add(dr);
        {if (true) return temp;}
      break;
    default:
      jj_la1[2] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public List<Node> FunctionParameters() throws ParseException {
    Node f;
    List<Node> fpara = new ArrayList();
    jj_consume_token(LPAREN);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case VOID:
      jj_consume_token(VOID);
      break;
    case CHAR:
    case INT:
      f = Declaration();
              fpara.add(f);
      label_2:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case COMMA:
          ;
          break;
        default:
          jj_la1[3] = jj_gen;
          break label_2;
        }
        jj_consume_token(COMMA);
        f = Declaration();
              fpara.add(f);
      }
      break;
    default:
      jj_la1[4] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RPAREN);
               {if (true) return fpara;}
    throw new Error("Missing return statement in function");
  }

  static final public Node Declaration() throws ParseException {
    Node dr, bt, temp;
    bt = BaseType1();
    dr = Declarator();
       temp = new Node(Id.VARDEC);
       temp.add(bt);
       temp.add(dr);
       temp.setPosition(dr.getPosition());
       {if (true) return temp;}
    throw new Error("Missing return statement in function");
  }

  static final public List<Node> AbstractFunctionParameters() throws ParseException {
    List<Node> l;
    l = FunctionParameters();
     {if (true) return l;}
    throw new Error("Missing return statement in function");
  }

  static final public Node BaseType1() throws ParseException {
    Token t;
    Node temp;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHAR:
      t = jj_consume_token(CHAR);
        temp = new Node(Id.CHAR);
        temp.setPosition(Position.fromToken(t));
        {if (true) return temp;}
      break;
    case INT:
      t = jj_consume_token(INT);
         temp = new Node(Id.INT);
         temp.setPosition(Position.fromToken(t));
         {if (true) return temp;}
      break;
    default:
      jj_la1[5] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public Node BaseType() throws ParseException {
    Node r;
    Token t;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHAR:
      t = jj_consume_token(CHAR);
         r = new Node(Id.CHAR);
         r.setPosition(Position.fromToken(t));
         {if (true) return r;}
      break;
    case INT:
      t = jj_consume_token(INT);
         r = new Node(Id.INT);
         r.setPosition(Position.fromToken(t));
         {if (true) return r;}
      break;
    case VOID:
      t = jj_consume_token(VOID);
         r = new Node(Id.VOID);
         r.setPosition(Position.fromToken(t));
         {if (true) return r;}
      break;
    default:
      jj_la1[6] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public Node Declarator() throws ParseException {
    Node id, r, temp;
    id = Identifier();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case LBRACK:
      jj_consume_token(LBRACK);
     temp = new Node(Id.ARR_TYPE);
     temp.add(id);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case INTEGER_CONSTANT:
        r = IntegerLiteral();
         temp.add(r);
        break;
      default:
        jj_la1[7] = jj_gen;
        ;
      }
      jj_consume_token(RBRACK);
     temp.setPosition(id.getPosition());
     {if (true) return temp;}
      break;
    default:
      jj_la1[8] = jj_gen;
     temp = new Node(Id.SCALAR_TYPE);
     temp.add(id);
     temp.setPosition(id.getPosition());
     {if (true) return temp;}
    }
    throw new Error("Missing return statement in function");
  }

  static final public Node CompoundStatement() throws ParseException {
    Node d, s, temp;
    List<Node> decs = new ArrayList<Node>();
    List<Node> stms = new ArrayList<Node>();
    Token t;
    t = jj_consume_token(LBRACE);
    label_3:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case CHAR:
      case INT:
        ;
        break;
      default:
        jj_la1[9] = jj_gen;
        break label_3;
      }
      d = Declaration();
      jj_consume_token(SEMI);
       decs.add(d);
    }
    label_4:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case IF:
      case RETURN:
      case WHILE:
      case NOT:
      case LPAREN:
      case MINUS:
      case SEMI:
      case LBRACE:
      case INTEGER_CONSTANT:
      case IDENT:
      case CHAR_CONSTANT:
        ;
        break;
      default:
        jj_la1[10] = jj_gen;
        break label_4;
      }
      s = Statement();
       stms.add(s);
    }
    jj_consume_token(RBRACE);
         temp = new CompoundStatementNode(decs, stms);
         temp.setPosition(Position.fromToken(t));
         {if (true) return temp;}
    throw new Error("Missing return statement in function");
  }

  static final public Node Statement() throws ParseException {
    Token t;
    Node c,s,s1,s2,temp;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case LBRACE:
      s = SimpleCompoundStatement();
   {if (true) return s;}
      break;
    case NOT:
    case LPAREN:
    case MINUS:
    case INTEGER_CONSTANT:
    case IDENT:
    case CHAR_CONSTANT:
      s = Expression();
      jj_consume_token(SEMI);
       temp = new Node(Id.STMNT);
       temp.add(s);
       temp.setPosition(s.getPosition());
       {if (true) return temp;}
      break;
    case SEMI:
      t = jj_consume_token(SEMI);
   {if (true) return new Node(Id.EMPTY_STMNT);}
      break;
    case IF:
      t = jj_consume_token(IF);
      jj_consume_token(LPAREN);
      c = Expression();
      jj_consume_token(RPAREN);
      s1 = Statement();
       temp = new Node(Id.IF);
       temp.add(c);
       temp.add(s1);
       temp.setPosition(Position.fromToken(t));
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case ELSE:
        jj_consume_token(ELSE);
        s2 = Statement();
       temp.add(s2);
        break;
      default:
        jj_la1[11] = jj_gen;
        ;
      }
       {if (true) return temp;}
      break;
    case WHILE:
      t = jj_consume_token(WHILE);
      jj_consume_token(LPAREN);
      c = Expression();
      jj_consume_token(RPAREN);
      s1 = Statement();
       temp = new Node(Id.WHILE);
       temp.add(c);
       temp.add(s1);
       temp.setPosition(Position.fromToken(t));
       {if (true) return temp;}
      break;
    case RETURN:
      t = jj_consume_token(RETURN);
   temp = new Node(Id.RETURN);
   temp.setPosition(Position.fromToken(t));
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NOT:
      case LPAREN:
      case MINUS:
      case INTEGER_CONSTANT:
      case IDENT:
      case CHAR_CONSTANT:
        c = Expression();
       temp.add(c);
        break;
      default:
        jj_la1[12] = jj_gen;
        ;
      }
      jj_consume_token(SEMI);
       {if (true) return temp;}
      break;
    default:
      jj_la1[13] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public Node SimpleCompoundStatement() throws ParseException {
    List<Node> stms = new ArrayList<Node>();
    Node s, temp;
    Token t;
    t = jj_consume_token(LBRACE);
    label_5:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case IF:
      case RETURN:
      case WHILE:
      case NOT:
      case LPAREN:
      case MINUS:
      case SEMI:
      case LBRACE:
      case INTEGER_CONSTANT:
      case IDENT:
      case CHAR_CONSTANT:
        ;
        break;
      default:
        jj_la1[14] = jj_gen;
        break label_5;
      }
      s = Statement();
       stms.add(s);
    }
    jj_consume_token(RBRACE);
       temp = new SimpleCompoundStatementNode(stms);
       temp.setPosition(Position.fromToken(t));
       {if (true) return temp;}
    throw new Error("Missing return statement in function");
  }

  static final public Node Expression() throws ParseException {
    Node exp, id, temp;
    exp = E1();
            {if (true) return exp;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E1() throws ParseException {
    Node left, right;
    Token t;
    left = E2();
    label_6:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case ANDAND:
        ;
        break;
      default:
        jj_la1[15] = jj_gen;
        break label_6;
      }
      t = jj_consume_token(ANDAND);
      right = E2();
                      left = new BinaryNode(Binop.ANDAND, left, right);
                      left.setPosition(Position.fromToken(t));
    }
         {if (true) return left;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E2() throws ParseException {
    Node left, right;
    Token t;
    left = E3();
    label_7:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NOTEQ:
      case EQEQ:
        ;
        break;
      default:
        jj_la1[16] = jj_gen;
        break label_7;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case EQEQ:
        t = jj_consume_token(EQEQ);
        right = E3();
                      left = new BinaryNode(Binop.EQEQ, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      case NOTEQ:
        t = jj_consume_token(NOTEQ);
        right = E3();
                      left = new BinaryNode(Binop.NE, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      default:
        jj_la1[17] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
         {if (true) return left;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E3() throws ParseException {
     Node left, right;
     Token t;
    left = E4();
    label_8:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LTEQ:
      case LT:
      case GTEQ:
      case GT:
        ;
        break;
      default:
        jj_la1[18] = jj_gen;
        break label_8;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LT:
        t = jj_consume_token(LT);
        right = E4();
                      left = new BinaryNode(Binop.LT, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      case GT:
        t = jj_consume_token(GT);
        right = E4();
                       left = new BinaryNode(Binop.GT, left, right);
                       left.setPosition(Position.fromToken(t));
        break;
      case LTEQ:
        t = jj_consume_token(LTEQ);
        right = E4();
                      left = new BinaryNode(Binop.LTEQ, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      case GTEQ:
        t = jj_consume_token(GTEQ);
        right = E4();
                      left = new BinaryNode(Binop.GTEQ, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      default:
        jj_la1[19] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
         {if (true) return left;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E4() throws ParseException {
    Node left, right;
    Token t;
    left = E5();
    label_9:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
      case MINUS:
        ;
        break;
      default:
        jj_la1[20] = jj_gen;
        break label_9;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case PLUS:
        t = jj_consume_token(PLUS);
        right = E5();
                      left = new BinaryNode(Binop.PLUS, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      case MINUS:
        t = jj_consume_token(MINUS);
        right = E5();
                      left = new BinaryNode(Binop.MINUS, left, right);
                     left.setPosition(Position.fromToken(t));
        break;
      default:
        jj_la1[21] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
        {if (true) return left;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E5() throws ParseException {
    Node left, right, temp;
    Token t;
    left = E6();
    label_10:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case MUL:
      case DIV:
        ;
        break;
      default:
        jj_la1[22] = jj_gen;
        break label_10;
      }
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case MUL:
        t = jj_consume_token(MUL);
        right = E6();
                      left = new BinaryNode(Binop.MUL, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      case DIV:
        t = jj_consume_token(DIV);
        right = E6();
                      left = new BinaryNode(Binop.DIV, left, right);
                      left.setPosition(Position.fromToken(t));
        break;
      default:
        jj_la1[23] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
    }
         {if (true) return left;}
    throw new Error("Missing return statement in function");
  }

  static final public Node E6() throws ParseException {
    Node id, num, exp, temp, cha, arr;
    List<Node> args = new ArrayList<Node>();
    Token t;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NOT:
      t = jj_consume_token(NOT);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LPAREN:
        jj_consume_token(LPAREN);
        exp = Expression();
        jj_consume_token(RPAREN);
                temp = new UnaryNode(Unop.NOT, exp);
                temp.setPosition(Position.fromToken(t));
                {if (true) return temp;}
        break;
      case IDENT:
        id = Identifier();
                 temp = new UnaryNode(Unop.NOT, id);
                 temp.setPosition(Position.fromToken(t));
                 {if (true) return temp;}
        break;
      case INTEGER_CONSTANT:
        num = IntegerLiteral();
                temp =  new UnaryNode(Unop.NOT, num);
                temp.setPosition(Position.fromToken(t));
                {if (true) return temp;}
        break;
      default:
        jj_la1[24] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    case MINUS:
      t = jj_consume_token(MINUS);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LPAREN:
        jj_consume_token(LPAREN);
        exp = Expression();
        jj_consume_token(RPAREN);
                        temp = new UnaryNode(Unop.NEG, exp);
                        temp.setPosition(Position.fromToken(t));
                        {if (true) return temp;}
        break;
      case IDENT:
        id = Identifier();
                        temp = new UnaryNode(Unop.NEG, id);
                        temp.setPosition(Position.fromToken(t));
                        {if (true) return temp;}
        break;
      case INTEGER_CONSTANT:
        num = IntegerLiteral();
                        temp = new UnaryNode(Unop.NEG, num);
                        temp.setPosition(Position.fromToken(t));
                        {if (true) return temp;}
        break;
      default:
        jj_la1[25] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
    case IDENT:
      id = Identifier();
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case LBRACK:
        jj_consume_token(LBRACK);
        exp = Expression();
        jj_consume_token(RBRACK);
             arr = new Node(Id.ARRAY);
             arr.add(id);
             arr.add(exp);
             arr.setPosition(id.getPosition());
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case EQ:
          t = jj_consume_token(EQ);
          exp = Expression();
               temp = new Node(Id.ASSIGN);
               temp.add(arr);
               temp.add(exp);
               temp.setPosition(Position.fromToken(t));
               {if (true) return temp;}
          break;
        default:
          jj_la1[26] = jj_gen;
               {if (true) return arr;}
        }
        break;
      case LPAREN:
        jj_consume_token(LPAREN);
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case NOT:
        case LPAREN:
        case MINUS:
        case INTEGER_CONSTANT:
        case IDENT:
        case CHAR_CONSTANT:
          exp = Expression();
            args.add(exp);
          label_11:
          while (true) {
            switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
            case COMMA:
              ;
              break;
            default:
              jj_la1[27] = jj_gen;
              break label_11;
            }
            jj_consume_token(COMMA);
            exp = Expression();
                args.add(exp);
          }
          break;
        default:
          jj_la1[28] = jj_gen;

        }
        jj_consume_token(RPAREN);
             temp =  new FCallNode(id, args);
             temp.setPosition(id.getPosition());
             {if (true) return temp;}
        break;
      case EQ:
        t = jj_consume_token(EQ);
        exp = Expression();
               temp = new Node(Id.ASSIGN);
               temp.add(id);
               temp.add(exp);
               temp.setPosition(Position.fromToken(t));
               {if (true) return temp;}
        break;
      default:
        jj_la1[29] = jj_gen;

      }
         {if (true) return id;}
      break;
    case LPAREN:
      jj_consume_token(LPAREN);
      exp = Expression();
      jj_consume_token(RPAREN);
         {if (true) return exp;}
      break;
    case INTEGER_CONSTANT:
      num = IntegerLiteral();
       {if (true) return num;}
      break;
    case CHAR_CONSTANT:
      cha = CharLiteral();
       {if (true) return cha;}
      break;
    default:
      jj_la1[30] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public Node Identifier() throws ParseException {
    Token t;
    t = jj_consume_token(IDENT);
       {if (true) return new IdentifierNode(t.image, Position.fromToken(t));}
    throw new Error("Missing return statement in function");
  }

  static final public Node IntegerLiteral() throws ParseException {
    Token t;
    Node n;
    t = jj_consume_token(INTEGER_CONSTANT);
       {if (true) return new IntegerLiteralNode(Integer.parseInt(t.image), Position.fromToken(t));}
    throw new Error("Missing return statement in function");
  }

  static final public Node CharLiteral() throws ParseException {
    Token t;
    Node n;
    t = jj_consume_token(CHAR_CONSTANT);
         {if (true) return new CharLiteralNode(t.image, Position.fromToken(t));}
    throw new Error("Missing return statement in function");
  }

  static private boolean jj_initialized_once = false;
  /** Generated Token Manager. */
  static public UcParseTokenManager token_source;
  static SimpleCharStream jj_input_stream;
  /** Current token. */
  static public Token token;
  /** Next token. */
  static public Token jj_nt;
  static private int jj_ntk;
  static private int jj_gen;
  static final private int[] jj_la1 = new int[31];
  static private int[] jj_la1_0;
  static private int[] jj_la1_1;
  static {
      jj_la1_init_0();
      jj_la1_init_1();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x5200,0x8000000,0x8100000,0x1000000,0x5200,0x1200,0x5200,0x0,0x0,0x1200,0xa12a800,0x400,0x2120000,0xa12a800,0xa12a800,0x40000,0x40010000,0x40010000,0x30000000,0x30000000,0x2800000,0x2800000,0x4400000,0x4400000,0x100000,0x100000,0x80000000,0x1000000,0x2120000,0x80100000,0x2120000,};
   }
   private static void jj_la1_init_1() {
      jj_la1_1 = new int[] {0x0,0x10,0x0,0x0,0x0,0x0,0x0,0x400,0x4,0x0,0x1c10,0x0,0x1c00,0x1c10,0x1c10,0x0,0x0,0x0,0x3,0x3,0x0,0x0,0x0,0x0,0xc00,0xc00,0x0,0x0,0x1c00,0x4,0x1c00,};
   }

  /** Constructor with InputStream. */
  public UcParse(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public UcParse(java.io.InputStream stream, String encoding) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser.  ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new UcParseTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public UcParse(java.io.Reader stream) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new UcParseTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public UcParse(UcParseTokenManager tm) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(UcParseTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 31; i++) jj_la1[i] = -1;
  }

  static private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  static final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  static final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  static private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  static private int[] jj_expentry;
  static private int jj_kind = -1;

  /** Generate ParseException. */
  static public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[45];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 31; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
          if ((jj_la1_1[i] & (1<<j)) != 0) {
            la1tokens[32+j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 45; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  static final public void enable_tracing() {
  }

  /** Disable tracing. */
  static final public void disable_tracing() {
  }

}
