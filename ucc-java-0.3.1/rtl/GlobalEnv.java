import java.util.*;

class GlobalEnv implements Env {
    Map<String, Type> global =  new HashMap<String, Type>();
    Map<String, LocalEnv> locals = new HashMap<String, LocalEnv>();
    
    public void insert(String s, Type t, Position p) {
	
	
	if (global.get(s) != null)
	    Semantic.error("Identifier \""+s+"\" doubly defined at position " + p);
	global.put(s,t);
    }
    

    public Type lookup(String s) {
	
	return global.get(s);
    }

    public void setResult(Type t) {
	throw new IllegalStateException("No result type in global environment");
    }

    public Type getResult() {
	throw new IllegalStateException("No result type in global environment");
    }

    public LocalEnv getEnv(String s){
	return locals.get(s);
    }
    
    public Env enter(String s) {
	LocalEnv lenv = new LocalEnv(this);
	locals.put(s, lenv);
	return lenv;
    }
}
