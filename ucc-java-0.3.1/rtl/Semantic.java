import java.util.*;

class Semantic {
    static boolean programFlag = true;
    
    static void error(String e){
	System.out.println("\n Error: " + e + "\n");
	programFlag = false;
    }
    
    static SemanticResult checkProgram(List<Node> program){
	GlobalEnv env = new GlobalEnv();
	
	for(Node td : program){
	    boolean tdFlag = checkTd(td, env);
	    programFlag = tdFlag && programFlag;
	}
	return new SemanticResult(programFlag, env);
	
    }

    static boolean checkTd(Node td, Env env){
	
	boolean flag = true;
	switch(td.getId()){
	case FUNC:
	    return checkFunc((FuncNode)td, env);
	case EXTERN:
	    
	    return checkExtern((ExternNode)td, env);
	case VARDEC:
	    
	    return checkVarDec(td, env);
	default:
	    error("Unexpected Toplevel Declaration: " + td.getId() +
		  " @" + td.getPosition());
	    return false;
	}
    }
    
    static boolean checkExtern(ExternNode extDec, Env env){
	Node ident = extDec.getIdent();
	Node r_type = extDec.getReturnType();
	List<Node> formals = extDec.getFormals();
	switch(ident.getId()){
	case SCALAR_TYPE:
	    
	    Type t = new FuncType(r_type.getId(), "extern");
	    IdentifierNode id = (IdentifierNode)ident.getChild(0);
	    env.insert(id.getName(), t, id.getPosition());

	    boolean flag = checkFormals(formals, env.enter("Ex" + id.getName()), (FuncType)t);
	    return flag;
	default:
	    error("Invalid extern declaration" + ident.getId());
	    return false;
	}
    }

    static boolean checkFunc(FuncNode funDec, Env env) {
	boolean flag, bodyFlag;
	Node ident = funDec.getIdent();
	Node r_type = funDec.getReturnType();
	List<Node> formals = funDec.getFormals();
	CompoundStatementNode body = (CompoundStatementNode)funDec.getBody();
	switch(ident.getId()){
	case SCALAR_TYPE:
	    
	    Type t = new FuncType(r_type.getId(), "func");
	    IdentifierNode id = (IdentifierNode)ident.getChild(0);
	    
	    Type s = env.lookup(id.getName());
	    if(s != null && s.isFunc() == false) {
		error("Redefintion");
		return false;
	    }
	    if(s != null && ((FuncType)s).getType().equals("extern")){
		if(r_type.getId() != ((FuncType)s).getId()){
		    error("Extern does not match func");
		    return false;
		}
		Env lenv = env.enter(id.getName());
		flag = checkFuncExtern(formals, (FuncType)s, lenv);
		if(flag == false){
		    error("Func does not match extern");
		    return false;
		}
		((FuncType)s).setType("func");
		lenv.setResult(s); 
		bodyFlag = checkBody(body, lenv);
		return flag && bodyFlag;
	    }
	    env.insert(id.getName(), t, id.getPosition());
	    Env lenv = env.enter(id.getName());
	    
	    lenv.setResult(t); // Local env result

	    boolean formalsFlag = checkFormals(formals, lenv , (FuncType)t);
	    bodyFlag = checkBody(body, lenv);
	    return formalsFlag && bodyFlag;
	default:
	    error("Invalid extern declaration" + ident.getId() +
		  " @" + funDec.getPosition());
	    return false;
	}
    }
    
    static boolean checkFuncExtern(List<Node> formals, FuncType ext, Env env){
	boolean flag = true;
	int i = 0;
	if(formals.size() != ext.argLength()){
	    error("Number of arguments differs between declarations");
	    return false;
	}
	for(Node dec : formals){
	    flag = flag && checkFormalDec(dec, env);
	    Type t = ((LocalEnv)env).getLatestExp();
	    if(t.getId() != ext.getArg(i).getId()){
		error("Argument type does not match @" + dec.getPosition());
		return false;
	    }
	    i++;
	}
	return flag;
    }

    static boolean checkFormals(List<Node> formals, Env env, FuncType t){
	boolean flag = true;
	for(Node dec : formals){
	    flag = flag && checkFormalDec(dec, env);
	    t.addArg(((LocalEnv)env).getLatestExp());
	}
	return flag;
    }
    
    static boolean checkVarDec(Node varDec, Env env){
	
	Node bt = varDec.getChild(0,2);
	Node dr = varDec.getChild(1,2);
	Type t;
	switch(dr.getId()){
	case SCALAR_TYPE:
	    t = new Type(bt.getId());
	    
	    break;
	case ARR_TYPE:
	    int children = dr.numberOfChildren();
	    if(children == 1){
		error("Arrays must be defined with a size @" + 
		      dr.getPosition());
		return false;
	    }
	    switch(bt.getId()){
		
	    case INT:
		t = new Type(Id.ARR_INT);
	        
		break;
	    case CHAR:
		t = new Type(Id.ARR_CHAR);
	     
		break;
	    default:
		error("Unknown type");
		return false;
	    }
	    break;
	default:
	    error("Unexpected Variable Declaration: " + varDec.getId() +
		  " @" + varDec.getPosition());
	    return false;
	}
	IdentifierNode id = (IdentifierNode)dr.getChild(0);
	env.insert(id.getName(), t, id.getPosition());
	return true;
    }

    static boolean checkBodyDec(Node varDec, Env env){
	
	Node bt = varDec.getChild(0,2);
	Node dr = varDec.getChild(1,2);
	Type t;
	switch(dr.getId()){
	case SCALAR_TYPE:
	    t = new Type(bt.getId());
	    
	    break;
	case ARR_TYPE:
	    int children = dr.numberOfChildren();
	    if(children == 1){
		error("Arrays must be defined with a size @" + 
		      dr.getPosition());
		return false;
	    }
	    switch(bt.getId()){
		
	    case INT:
		t = new Type(Id.ARR_INT);
	        
		break;
	    case CHAR:
		t = new Type(Id.ARR_CHAR);
	     
		break;
	    default:
		error("Unknown type");
		return false;
	    }
	    break;
	default:
	    error("Unexpected Variable Declaration: " + varDec.getId() +
		  " @" + varDec.getPosition());
	    return false;
	}
	t.setIsLocal();
	IdentifierNode id = (IdentifierNode)dr.getChild(0);
	env.insert(id.getName(), t, id.getPosition());
	return true;
    }

    static boolean checkFormalDec(Node varDec, Env env){
	
	Node bt = varDec.getChild(0,2);
	Node dr = varDec.getChild(1,2);
	Type t;
	switch(dr.getId()){
	case SCALAR_TYPE:
	    t = new Type(bt.getId());
	    
	    break;
	case ARR_TYPE:
	    switch(bt.getId()){
	    case INT:
		t = new Type(Id.ARR_INT);
		
		break;
	    case CHAR:
		t = new Type(Id.ARR_CHAR);
		
		break;
	    default:
		error("Unknown type @" + bt.getPosition());
		return false;
	    }
	    break;
	default:
	    error("Unexpected Variable Declaration: " + varDec.getId() +
		  " @" + varDec.getPosition());
	    return false;
	}
	IdentifierNode id = (IdentifierNode)dr.getChild(0);
	t.setIsLocal();
	env.insert(id.getName(), t, id.getPosition());
	((LocalEnv)env).setLatestExp(t);
	return true;
    }
    
    static boolean checkBody(CompoundStatementNode body, Env env){
	boolean flag = true;
	List<Node> declarations = body.getDeclarations();
	List<Node> statements = body.getStatements();
	for(Node dec : declarations){
	    flag = checkBodyDec(dec, env);
	}
	for(Node stm : statements){
	    flag = checkStatement(stm, env);
	}
	return flag;
    }
    static boolean checkStatement(Node stm, Env env){
	boolean flag = true;
	boolean flag1 = true;
	Type b;
	switch(stm.getId()){
	case SIMPLE_COMPOUND_STMNT:
	    List<Node> stms = ((SimpleCompoundStatementNode)stm).getStatements();
	    for(Node s : stms){
		flag = flag && checkStatement(s, env);
	    }
	    return flag;
	case STMNT:
	    
	    return checkExpression(stm.getChild(0), env);
	case EMPTY_STMNT:
	    
	    return true;
	case IF:
	    int n = stm.numberOfChildren();
	    flag1 = checkExpression(stm.getChild(0,n), env);
	    b = ((LocalEnv)env).getLatestExp();
	    if(b.getId() != Id.INT && b.getId() != Id.CHAR){
		error("Expected INT or CHAR in if condition, found " + b + 
		      " @" + stm.getPosition());
		return false;
	    }

	    boolean flag2 = checkStatement(stm.getChild(1,n), env);
	    if(n==3)
		flag = checkStatement(stm.getChild(2,n), env);
	    return flag && flag1 && flag2;  
	case WHILE:
	    flag1 = checkExpression(stm.getChild(0,2), env);
	    b = ((LocalEnv)env).getLatestExp();
	    if(b.getId() != Id.INT && b.getId() != Id.CHAR){
		error("Expected INT or CHAR in while condition, found " + b + 
		      " @" + stm.getPosition());
		return false;
	    }
	    flag = checkStatement(stm.getChild(1,2), env);
	    return flag && flag1;
	case RETURN:
	    return checkReturn(stm, env);
	default:
	    error("Invalid statement @" + stm.getPosition());
	    return false;
	}
    }
    
    static boolean checkReturn(Node r, Env env){
	boolean flag = true;
	int n = r.numberOfChildren();
	Type rType = env.getResult();
	Type t;
	if(rType.getId() != Id.VOID && n == 0){
	    error("Invalid return type, expected " + rType.getId() + " got VOID.");
	    return false;
	}
	if(n==1){
	    flag = checkExpression(r.getChild(0), env);
	    if(flag == false) return false;
	    t = ((LocalEnv)env).getLatestExp();
	    if(!(rType.getId() == Id.INT && t.getId() == Id.CHAR) && rType.getId() != t.getId()) {
		error("Invalid return type, expected "+ rType.getId() 
		      + " got " + t.getId() + " @" + r.getPosition());
		return false;
	    }
	}
	return true;
    }
    
    static boolean checkExpression(Node exp, Env env){
	boolean flag = true;
	switch(exp.getId()){
	case BINARY:
	    
	    return checkBinary((BinaryNode)exp, env);
	case UNARY:
	    
	    return checkUnary((UnaryNode)exp, env);
	case FCALL:
	    
	    return checkFCall((FCallNode)exp, env);
	case ASSIGN:
	    
	    
	    flag = checkAssign(exp, env);
	    return flag;
	case CHAR_LITERAL:
	    
	    ((LocalEnv)env).setLatestExp(new Type(Id.CHAR));
	    return true;
	case INTEGER_LITERAL:
	    ((LocalEnv)env).setLatestExp(new Type(Id.INT));
	    
	    return true;
	case IDENT:
	    
	    String name = ((IdentifierNode)exp).getName();
	    Type identType = env.lookup(name);
	    
	    ((LocalEnv)env).setLatestExp(identType);
	    if(identType == null){
		error("Variable " + name + " not defined @"
		      + exp.getPosition());
		return false;
	    }
	    return true;
	case ARRAY:
	    
	    IdentifierNode id = (IdentifierNode)exp.getChild(0,2);
	    Type t = env.lookup(id.getName());
	    if(t.getId() != Id.ARR_CHAR && t.getId() != Id.ARR_INT){
		error("Can not index an " + t.getId() + 
		      " @" + exp.getPosition());
		return false;
	    }
	    if(t.getId() == Id.ARR_CHAR){
		t = new Type(Id.CHAR);
	    }else{
		t = new Type(Id.INT);
	    }
	    ((LocalEnv)env).setLatestExp(t);
	    return true;
	default:
	    error("Invalid expression @" + exp.getPosition());
	    return false;
	}
    }

    static boolean checkAssign(Node exp, Env env){
	boolean flag = true;
	Node var = exp.getChild(0,2);
	IdentifierNode var2;
	Type expType, t;
	if(var.getId() == Id.ARRAY){
	    var2 = (IdentifierNode)var.getChild(0,2);
	    if(!checkExpression(var.getChild(1,2), env) ||
	       !(((LocalEnv)env).getLatestExp().getId() == Id.INT) &&
	       !(((LocalEnv)env).getLatestExp().getId() == Id.CHAR)) {
		error("Felet");
		return false;
	    }
	    t = env.lookup(var2.getName());
	    if(t.isFunc()){
		error(var2.getName() + " is a function @"
		      + var2.getPosition());
	    }
	    if(t.getId() == Id.ARR_CHAR){
		t = new Type(Id.CHAR);
	    }else{
		t = new Type(Id.INT);
	    }
	} else {
	    var2 = (IdentifierNode) var;
	    t = env.lookup(var2.getName());
	    if(t.isFunc()){
		error(var2.getName() + " is a function @" +
		      var2.getPosition());
	    }
	}
	flag = checkExpression(exp.getChild(1,2), env);
	expType = ((LocalEnv)env).getLatestExp();
	if(t==null || expType == null){
	    error("Variable " + var2.getName() + " not defined @" +
		  var2.getPosition());
	    return false;
	}

	// if((t.getId() != expType.getId() &&
	//     !(t.getId() == Id.INT && expType.getId() == Id.CHAR)) ||
	//    (t.getId() == expType.getId() && (t.getId() == Id.ARR_CHAR) ||
	//     t.getId() == Id.ARR_INT)) {

	if(!(t.getId() == expType.getId()) && ((t.getId() == Id.ARR_CHAR) ||
					       (t.getId() == Id.ARR_INT))) {
	    error("Type missmatch, expected " + t.getId() + " at " + var2.getPosition() + " got " + expType.getId());
	    return false;
	}
	return flag;
    }
    static boolean checkFCall(FCallNode fc, Env env){
	boolean flag = true;
	IdentifierNode id = (IdentifierNode)fc.getIdent();
	Type t = env.lookup(id.getName());
	if(t == null){
	    error("The function " +id.getName() + 
		  " does not exists @" + id.getPosition());
	    return false;
	}
	if(!t.isFunc()){
	    error(id.getName() + " is not a function @" + id.getPosition());
	    return false;
	}
	List<Node> args = fc.getArgs();
	if(((FuncType)t).argLength() != args.size()){
	    error("Wrong number of arguments @" + id.getPosition());
	    return false;
	}
	int i = 0;
	for(Node exp : args){
	    flag = flag && checkExpression(exp, env);
	    Type expType = ((LocalEnv)env).getLatestExp();
	    if(expType.getId() != ((FuncType)t).getArg(i).getId() &&
	       !(expType.getId() == Id.CHAR || expType.getId() == Id.INT) &&
	       !(((FuncType)t).getArg(i).getId() == Id.CHAR || 
		 ((FuncType)t).getArg(i).getId() == Id.INT)){
		error("Argument type missmatch, expected " + 
		      ((FuncType)t).getArg(i).getId() +
		      " got " + expType.getId() + 
		      " @" + exp.getPosition());
		return false;
	    }
	    i++;
	}
	((LocalEnv)env).setLatestExp(t);
	return flag;
    }

    static boolean checkBinary(BinaryNode b, Env env){
	boolean flag = true;
	boolean right, left;
	Id lId, rId;
	Type l, r;
	switch(b.getOp()){
	case ANDAND:
	case EQEQ:
	case NE:
	case LT:
	case GT:
	case LTEQ:
	case GTEQ:
	    left = checkExpression(b.getChild(0,2), env);
	    if ( left == false) return false;
	    l = ((LocalEnv)env).getLatestExp();
	    right = checkExpression(b.getChild(1,2), env);
	    if ( right == false) return false;
	    r = ((LocalEnv)env).getLatestExp();
	    lId = l.getId();
	    rId = r.getId();
	    if(lId == Id.ARR_CHAR || lId == Id.ARR_INT ||
	       rId == Id.ARR_CHAR || rId == Id.ARR_INT){
		error("Incompatable type, can not evaluate " + lId +
		      " or " + rId + " @" + b.getPosition());
		return false;
	    }
	    if(rId == Id.INT || lId == Id.INT){
		((LocalEnv)env).setLatestExp(new Type(Id.INT));
		return true;
	    }
	    ((LocalEnv)env).setLatestExp(new Type(Id.CHAR));
	    return true;
	case PLUS:
	case MUL:
	case DIV:
	case MINUS:
	    left = checkExpression(b.getChild(0,2), env);
	    if ( left == false){ 
		//error("Check failed @" + b.getPosition());
		return false;
	    }
	    l = ((LocalEnv)env).getLatestExp();
	    right = checkExpression(b.getChild(1,2), env);
	    if ( right == false) {
		//error("Check failed @" + b.getPosition());
		return false;
	    }
	    r = ((LocalEnv)env).getLatestExp();
	    lId = l.getId();
	    rId = r.getId();
	    if((lId != rId) && (lId == Id.INT && rId == Id.CHAR) || 
	       (lId == Id.CHAR && rId == Id.INT) && rId != Id.ARR_CHAR 
	       && rId != Id.ARR_INT && lId != Id.ARR_CHAR && lId != Id.ARR_INT) {
		((LocalEnv)env).setLatestExp(new Type(Id.INT));
		return true;
	    } else if((lId == rId) && ((lId == Id.INT) || (lId == Id.CHAR))) {
		((LocalEnv)env).setLatestExp(new Type(l.getId()));
	
		return true;
	    }
	    error("Incompatable types cannot use arithmetic operations on "
		  + lId + " and " + rId +
		  " @" + b.getPosition());
	    return false;
	default:
	    error("Invalid binary expression @" + b.getPosition());
	    return false;
	}
    }

    static boolean checkUnary(UnaryNode exp, Env env){
	boolean flag = true;
	Type t;
	Id tId;
	switch(exp.getOp()){
	case NOT:
	    flag = checkExpression(exp.getChild(0), env);
	    if(flag == false) {
		error("Incompatable type found @" + exp.getPosition());
		return false;
	    }
	    t = ((LocalEnv)env).getLatestExp();
	    tId = t.getId();
	    if(tId != Id.INT && tId != Id.CHAR){
		error("Incompatable type, found " + tId + 
		      " expected INT or CHAR @" + exp.getPosition());
		return false;
	    }
	    return true;
	case NEG:
	    flag = checkExpression(exp.getChild(0), env);
	    if(flag == false) {
		error("Incompatable type found @" + exp.getPosition());
		return false;
	    }
	    t = ((LocalEnv)env).getLatestExp();
	    tId = t.getId();
	    if(tId != Id.INT){
		error("Incompatable type, found " + tId + " expected INT @" +
		      exp.getPosition());
		return false;
	    }
	    return true;
	default:
	    error("Invalid unary expression @" + exp.getPosition());
	    return false;
	}
    }
}
