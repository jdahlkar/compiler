
# END OF RTL
.data

.text
# Function start:
f:
sw $fp, 20($sp)
addiu $fp, $sp, 16
addiu $sp, $sp, 0
sw $fp, -4($fp)
sw $ra, 0($fp)
# Eval start:
# Binary Start
lw $t0, -12($fp)
lw $t1, -8($fp)
add $t2, $t0, $t1
sw $t2 -16($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -16($fp)
j end_f
# TempExp End
# Eval end
end_f:
lw $ra, 0($fp)
addiu $sp, $fp, 4
lw $fp, 4($fp)
jr $ra
# Function end
# Function start:
main:
sw $fp, 12($sp)
addiu $fp, $sp, 0
addiu $sp, $sp, -8
addiu $sp, $sp, 0
sw $fp, -4($fp)
sw $ra, 0($fp)
# Eval start:
# Icon Start
li $t0, 2
sw $t0 -8($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 3
sw $t0 -12($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Call start:
addiu $sp, $sp, -12
lw $t0, -8($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
lw $t0, -12($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal f
sw $v0, -16($fp)
addiu $sp, $sp, -4
# Call end
end_main:
lw $ra, 0($fp)
addiu $sp, $fp, 4
lw $fp, 4($fp)
jr $ra
# Function end

.data
.globl putint 
.globl putstring
.globl getint
.globl getstring

.text

putint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 1
syscall
lw        $fp, 4($sp)
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

putstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 4
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
li 	  $v0, 5
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li        $a1, 80
li 	  $v0, 8
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra
