
# END OF RTL
.data

.text
# Function start:
# Function start:
# Function start:
fib:
addiu $fp, $sp, 12
sw $ra, -4($fp)
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -12($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -12($fp)
lw $t1, -8($fp)
xor $t2, $t0, $t1
sltiu $t2, $t2, 1
sw $t2 -16($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# CJump start:
lw $t0, -16($fp)
beq $t0, $zero, end0
# CJump end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -20($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -20($fp)
# TempExp End
# Eval end
end0:
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -24($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -24($fp)
lw $t1, -8($fp)
xor $t2, $t0, $t1
sltiu $t2, $t2, 1
sw $t2 -28($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# CJump start:
lw $t0, -28($fp)
beq $t0, $zero, end1
# CJump end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -32($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -32($fp)
# TempExp End
# Eval end
end1:
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -36($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -36($fp)
lw $t1, -8($fp)
sub $t2, $t1, $t0
sw $t2 -40($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Call start:
sw $fp, 0($sp)
addiu $sp, $sp, -4
addiu $sp, $sp, -8
lw $t0, -40($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal fib
sw $v0, -44($fp)
# Call end
# Eval start:
# Icon Start
li $t0, 2
sw $t0 -48($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -48($fp)
lw $t1, -8($fp)
sub $t2, $t1, $t0
sw $t2 -52($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Call start:
sw $fp, 0($sp)
addiu $sp, $sp, -4
addiu $sp, $sp, -8
lw $t0, -52($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal fib
sw $v0, -56($fp)
# Call end
# Eval start:
# Binary Start
lw $t0, -56($fp)
lw $t1, -44($fp)
add $t2, $t0, $t1
sw $t2 -60($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -60($fp)
# TempExp End
# Eval end
lw $ra, -4($fp)
addiu $sp, $fp, 4
lw $fp, 0($sp)
jr $ra
# Function end
# Function start:
main:
addiu $fp, $sp, 16
sw $ra, -12($fp)
# Eval start:
# Icon Start
li $t0, 32
sw $t0 -20($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -24($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -28($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -24($fp)
lw $t1, -28($fp)
mul $t2, $t0, $t1
sw $t2 -32($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -36($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -32($fp)
lw $t1, -36($fp)
add $t2, $t0, $t1
sw $t2 -40($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Binary Start
lw $t0, -40($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -44($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Store start:
lw $t0, -20($fp)
lw $t1, -44($fp)
sb $t0, 0($t1)
# Store end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -48($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -52($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -56($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -52($fp)
lw $t1, -56($fp)
mul $t2, $t0, $t1
sw $t2 -60($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -64($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -60($fp)
lw $t1, -64($fp)
add $t2, $t0, $t1
sw $t2 -68($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Binary Start
lw $t0, -68($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -72($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Store start:
lw $t0, -48($fp)
lw $t1, -72($fp)
sb $t0, 0($t1)
# Store end
# Eval start:
# Icon Start
li $t0, 10
sw $t0 -76($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -80($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -84($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -80($fp)
lw $t1, -84($fp)
mul $t2, $t0, $t1
sw $t2 -88($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Icon Start
li $t0, 4
sw $t0 -92($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -88($fp)
lw $t1, -92($fp)
add $t2, $t0, $t1
sw $t2 -96($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Binary Start
lw $t0, -96($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -100($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Store start:
lw $t0, -76($fp)
lw $t1, -100($fp)
sb $t0, 0($t1)
# Store end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -104($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -108($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -112($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -108($fp)
lw $t1, -112($fp)
mul $t2, $t0, $t1
sw $t2 -116($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Icon Start
li $t0, 4
sw $t0 -120($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -116($fp)
lw $t1, -120($fp)
add $t2, $t0, $t1
sw $t2 -124($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# Binary Start
lw $t0, -124($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -128($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Store start:
lw $t0, -104($fp)
lw $t1, -128($fp)
sb $t0, 0($t1)
# Store end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -132($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Store start:
lw $t0, -132($fp)
sw $t0, -16($fp)
addiu $sp $sp -4
# Store end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -136($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Call start:
lw $t0, -136($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putint
sw $v0, -140($fp)
# Call end
loop2:
# Eval start:
# Icon Start
li $t0, 12
sw $t0 -144($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -144($fp)
lw $t1, -16($fp)
addi $t0, $t0, 1
slt $t2, $t1, $t0
sw $t2 -148($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# CJump start:
lw $t0, -148($fp)
beq $t0, $zero, stop2
# CJump end
# Call start:
lw $t0, -16($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putint
sw $v0, -152($fp)
# Call end
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -156($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -156($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -160($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Call start:
lw $t0, -160($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putstring
sw $v0, -164($fp)
# Call end
# Call start:
sw $fp, 0($sp)
addiu $sp, $sp, -4
addiu $sp, $sp, -8
lw $t0, -16($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal fib
sw $v0, -168($fp)
# Call end
# Call start:
lw $t0, -168($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putint
sw $v0, -172($fp)
# Call end
# Eval start:
# Icon Start
li $t0, 4
sw $t0 -176($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -176($fp)
lw $t1, -12($fp)
add $t2, $t0, $t1
sw $t2 -180($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Call start:
lw $t0, -180($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putstring
sw $v0, -184($fp)
# Call end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -188($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -188($fp)
lw $t1, -16($fp)
add $t2, $t0, $t1
sw $t2 -192($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Store start:
lw $t0, -192($fp)
sw $t0, -16($fp)
addiu $sp $sp -4
# Store end
j loop2
stop2:
lw $ra, -12($fp)
addiu $sp, $fp, 4
lw $fp, 0($sp)
jr $ra
# Function end

.data
.globl putint 
.globl putstring
.globl getint
.globl getstring

.text

putint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 1
syscall
lw        $fp, 4($sp)
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

putstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 4
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
li 	  $v0, 5
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li        $a1, 80
li 	  $v0, 8
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra
