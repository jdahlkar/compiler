/* Generated By:JavaCC: Do not edit this line. UcParseConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface UcParseConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int CHAR = 9;
  /** RegularExpression Id. */
  int ELSE = 10;
  /** RegularExpression Id. */
  int IF = 11;
  /** RegularExpression Id. */
  int INT = 12;
  /** RegularExpression Id. */
  int RETURN = 13;
  /** RegularExpression Id. */
  int VOID = 14;
  /** RegularExpression Id. */
  int WHILE = 15;
  /** RegularExpression Id. */
  int NOTEQ = 16;
  /** RegularExpression Id. */
  int NOT = 17;
  /** RegularExpression Id. */
  int ANDAND = 18;
  /** RegularExpression Id. */
  int OROR = 19;
  /** RegularExpression Id. */
  int LPAREN = 20;
  /** RegularExpression Id. */
  int RPAREN = 21;
  /** RegularExpression Id. */
  int MUL = 22;
  /** RegularExpression Id. */
  int PLUS = 23;
  /** RegularExpression Id. */
  int COMMA = 24;
  /** RegularExpression Id. */
  int MINUS = 25;
  /** RegularExpression Id. */
  int DIV = 26;
  /** RegularExpression Id. */
  int SEMI = 27;
  /** RegularExpression Id. */
  int LTEQ = 28;
  /** RegularExpression Id. */
  int LT = 29;
  /** RegularExpression Id. */
  int EQEQ = 30;
  /** RegularExpression Id. */
  int EQ = 31;
  /** RegularExpression Id. */
  int GTEQ = 32;
  /** RegularExpression Id. */
  int GT = 33;
  /** RegularExpression Id. */
  int LBRACK = 34;
  /** RegularExpression Id. */
  int RBRACK = 35;
  /** RegularExpression Id. */
  int LBRACE = 36;
  /** RegularExpression Id. */
  int RBRACE = 37;
  /** RegularExpression Id. */
  int DIGIT1 = 38;
  /** RegularExpression Id. */
  int DIGIT = 39;
  /** RegularExpression Id. */
  int ALPHA = 40;
  /** RegularExpression Id. */
  int ALPHANUM = 41;
  /** RegularExpression Id. */
  int INTEGER_CONSTANT = 42;
  /** RegularExpression Id. */
  int IDENT = 43;
  /** RegularExpression Id. */
  int CHAR_CONSTANT = 44;

  /** Lexical state. */
  int DEFAULT = 0;
  /** Lexical state. */
  int ML_COMMENT = 1;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\n\"",
    "\"\\r\"",
    "\"\\f\"",
    "<token of kind 6>",
    "\"/*\"",
    "<token of kind 8>",
    "\"char\"",
    "\"else\"",
    "\"if\"",
    "\"int\"",
    "\"return\"",
    "\"void\"",
    "\"while\"",
    "\"!=\"",
    "\"!\"",
    "\"&&\"",
    "\"||\"",
    "\"(\"",
    "\")\"",
    "\"*\"",
    "\"+\"",
    "\",\"",
    "\"-\"",
    "\"/\"",
    "\";\"",
    "\"<=\"",
    "\"<\"",
    "\"==\"",
    "\"=\"",
    "\">=\"",
    "\">\"",
    "\"[\"",
    "\"]\"",
    "\"{\"",
    "\"}\"",
    "<DIGIT1>",
    "<DIGIT>",
    "<ALPHA>",
    "<ALPHANUM>",
    "<INTEGER_CONSTANT>",
    "<IDENT>",
    "<CHAR_CONSTANT>",
  };

}
