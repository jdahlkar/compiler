class Store implements RtlInsn {
    private RtlType type;
    private int addr;
    private int val;
    private boolean arr = true;
    private boolean isLocal = true;
    public Store (RtlType _type, int _addr, int _val){
	type =_type;
	addr =_addr;
	val =_val;
    }

    public void setIsLocal(boolean local) {
	isLocal = local;
    }

    public boolean isLocal() {
	return isLocal;
    }

    public RtlType getType (){
	return type;
    }

    public boolean isArr() {
	return arr;
    }

    public void setArr(boolean _arr) {
	arr = _arr;
    }

    public void setType (RtlType _type){
	type =_type;
    }

    public int getAddr (){
	return addr;
    }

    public void setAddr (int _addr){
	addr =_addr;
    }

    public int getVal (){
	return val;
    }

    public void setVal (int _val){
	val =_val;
    }

    public String toString(){
	return "store" + "(" + type + " " + 
	    Rtl.regToString(addr) + " " + Rtl.regToString(val) + ")";
    };
}


