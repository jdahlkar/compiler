import java.util.*;

class Type{
    private Id id;
    private int reg;
    private int frameOffset;
    private boolean local = false;
    
    public Type(Id id){
	this.id = id;
    }
    
    public Id getId(){
	return id;
    }
    
    public boolean isFunc(){
	return false;
    }
    public void setReg(int reg){
	this.reg = reg;
    }
    public int getReg(){
	return reg;
    }
    public void setFrameOff(int fo){
	this.frameOffset = fo;
    }
    public int getFrameOff(){
	return frameOffset;
    }
    public String toString(){
	return id.toString();
    }
    public boolean isLocal(){
	return local;
    }
    public void setIsLocal(){
	local = true;
    }
}

class FuncType extends Type{
    private ArrayList<Type> args;
    private String type;
    public FuncType(Id id, String type){
	super(id);
	this.type = type;
	args = new ArrayList<Type>();
    }
    public void addArg(Type t){
	args.add(t);
    }
    public Type getArg(int i){
	return args.get(i);
    }
    public boolean isFunc(){
	return true;
    }
    public String getType(){
	return type;
    }
    public void setType(String type){
	this.type = type;
    }
    public int argLength(){
	return args.size();
    }
}
