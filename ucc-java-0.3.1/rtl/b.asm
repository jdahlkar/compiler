
proc putint 

proc getint 

proc putstring 

proc fac 
 formals: [#2 ]
 locals: [#3 #4 #5 #6 #7 #8 #9 #10 ]
 frame: 0
    eval(#4 icon(0))
    eval(#5 binary(EQ #2 #4))
    cjump(false #5 el0)
    eval(#6 icon(1))
    eval(RV tempexp(#6))
    jump(end0)
labdef(el0)
    eval(#7 icon(1))
    eval(#8 binary(MINUS #2 #7))
    call(#9 fac #8)
    eval(#10 binary(MUL #2 #9))
    eval(RV tempexp(#10))
labdef(end0)

proc main 
 formals: []
 locals: [#2 #3 #4 #5 ]
 frame: 0
    call(#3 getint )
    store(LONG #2 #3)
    call(#4 fac #2)
    call(#5 putint #4)

# END OF RTL
.data

.text
# Function start:
# Function start:
# Function start:
# Function start:
fac:
addiu $fp, $sp, 12
sw $ra, -4($fp)
# Eval start:
# Icon Start
li $t0, 0
sw $t0 -16($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -16($fp)
lw $t1, -8($fp)
xor $t2, $t0, $t1
sltiu $t2, $t2, 1
sw $t2 -20($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# CJump start:
lw $t0, -20($fp)
beq $t0, $zero, el0
# CJump end
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -24($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -24($fp)
# TempExp End
# Eval end
j end0
el0:
# Eval start:
# Icon Start
li $t0, 1
sw $t0 -28($fp)
addiu $sp $sp -4
# Icon End
# Eval end
# Eval start:
# Binary Start
lw $t0, -28($fp)
lw $t1, -8($fp)
sub $t2, $t1, $t0
sw $t2 -32($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Call start:
sw $fp, 0($sp)
addiu $sp, $sp, -8
lw $t0, -32($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal fac
sw $v0, -36($fp)
addiu $sp, $sp, -4
# Call end
# Eval start:
# Binary Start
lw $t0, -36($fp)
lw $t1, -8($fp)
mul $t2, $t0, $t1
sw $t2 -40($fp)
addiu $sp $sp -4
# Binary End
# Eval end
# Eval start:
# TempExp Start
lw $v0, -40($fp)
# TempExp End
# Eval end
end0:
lw $ra, -4($fp)
addiu $sp, $fp, 0
lw $fp, 0($fp)
jr $ra
# Function end
# Function start:
main:
addiu $fp, $sp, 8
sw $ra, -4($fp)
# Call start:
jal getint
sw $v0, -12($fp)
addiu $sp, $sp, -4
# Call end
# Store start:
lw $t0, -12($fp)
sw $t0, -8($fp)
addiu $sp $sp -4
# Store end
# Call start:
sw $fp, 0($sp)
addiu $sp, $sp, -8
lw $t0, -8($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal fac
sw $v0, -16($fp)
addiu $sp, $sp, -4
# Call end
# Call start:
lw $t0, -16($fp)
sw $t0, 0($sp)
addiu $sp $sp -4
jal putint
sw $v0, -20($fp)
addiu $sp, $sp, -4
# Call end
lw $ra, -4($fp)
addiu $sp, $fp, 0
lw $fp, 0($fp)
jr $ra
# Function end

.data
.globl putint 
.globl putstring
.globl getint
.globl getstring

.text

putint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 1
syscall
lw        $fp, 4($sp)
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

putstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li 	  $v0, 4
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getint:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
li 	  $v0, 5
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra

getstring:
addi      $sp, $sp, -24
sw        $fp, 4($sp)
sw        $ra, 8($sp)
addi      $fp, $sp, 24
lw        $a0, 4($fp)
li        $a1, 80
li 	  $v0, 8
syscall
lw        $fp, 4($sp)	
lw        $ra, 8($sp)
addi      $sp, $sp, 24
jr        $ra
