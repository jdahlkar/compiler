class Type{
    private Id id;
    
    public Type(Id id){
	this.id = id;
    }
    
    public Id getId(){
	return id;
    }
    
    public String toString(){
	return id.toString();
    }
}
