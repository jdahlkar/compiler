import java.util.*;

class LocalEnv implements Env {

    Map<String, Type> local =  new HashMap<String, Type>();
    
    Env globalEnv;
    Type latestExp;
    Type result;

    public void setLatestExp(Type t) {
	this.latestExp = t;
    }

    public Type getLatestExp() {
	return latestExp;
    }

    LocalEnv(Env _globalEnv) {
	globalEnv = _globalEnv;
    }
    
    public void insert(String s, Type t, Position p) {
	System.out.println("Locally inserting "+s);
	if (local.get(s) != null)
	    Semantic.error("Identifier \""+s+"\" doubly defined at position " + p);
	local.put(s,t);
    }
    

    public Type lookup(String s) {
	System.out.println("Local lookup: "+s);
	Type r = local.get(s);
	if (r!=null) return r;
	else return globalEnv.lookup(s);
    }

    public void setResult(Type t) {
	result = t;
    }

    public Type getResult() {
	return result;
    }

    public Env enter() {
	throw new IllegalStateException("Already in local environment");
    }
}
