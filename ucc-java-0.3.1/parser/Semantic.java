import java.util.*;

class Semantic {
    static boolean programFlag = true;
    
    static void error(String e){
	System.out.println(e);
	programFlag = false;
    }
    
    static boolean checkProgram(List<Node> program){
	GlobalEnv env = new GlobalEnv();
	
	for(Node td : program){
	    boolean tdFlag = checkTd(td, env);
	    programFlag = tdFlag && programFlag;
	}
	return programFlag;
	
    }

    static boolean checkTd(Node td, Env env){
	
	boolean flag = true;
	switch(td.getId()){
	case FUNC:
	    System.out.println("Function");
	    return checkFunc((FuncNode)td, env);
	case EXTERN:
	    System.out.println("Extern");
	    return checkExtern((ExternNode)td, env);
	case VARDEC:
	    System.out.println("Vardec");
	    return checkVarDec(td, env);
	default:
	    error("Unexpected Toplevel Declaration: " + td.getId());
	    return false;
	}
    }
    
    static boolean checkExtern(ExternNode extDec, Env env){
	Node ident = extDec.getIdent();
	Node r_type = extDec.getReturnType();
	List<Node> formals = extDec.getFormals();
	switch(ident.getId()){
	case SCALAR_TYPE:
	    System.out.println("Scalar extern");
	    Type t = new Type(r_type.getId());
	    IdentifierNode id = (IdentifierNode)ident.getChild(0);
	    env.insert(id.getName(), t, id.getPosition());

	    boolean flag = checkFormals(formals, env.enter());
	    return flag;
	default:
	    error("Invalid extern declaration" + ident.getId());
	    return false;
	}
    }

    static boolean checkFunc(FuncNode funDec, Env env) {
	Node ident = funDec.getIdent();
	Node r_type = funDec.getIdent();
	List<Node> formals = funDec.getFormals();
	CompoundStatementNode body = (CompoundStatementNode)funDec.getBody();
	switch(ident.getId()){
	case SCALAR_TYPE:
	    System.out.println("Scalar func");
	    Type t = new Type(r_type.getId());
	    IdentifierNode id = (IdentifierNode)ident.getChild(0);
	    // CHeck
	    env.insert(id.getName(), t, id.getPosition());
	    Env lenv = env.enter();
	    
	    lenv.setResult(t); // Local env result

	    boolean formalsFlag = checkFormals(formals, lenv);
	    boolean bodyFlag = checkBody(body, lenv);
	    return formalsFlag && bodyFlag;
	default:
	    error("Invalid extern declaration" + ident.getId());
	    return false;
	}
    }
    
    static boolean checkFormals(List<Node> formals, Env env){
	boolean flag = true;
	for(Node dec : formals){
	    flag = checkVarDec(dec, env);
	}
	return flag;
    }
    
    static boolean checkVarDec(Node varDec, Env env){
	
	Node bt = varDec.getChild(0,2);
	Node dr = varDec.getChild(1,2);
	Type t;
	switch(dr.getId()){
	case SCALAR_TYPE:
	    t = new Type(bt.getId());
	    System.out.println("Scalar");
	    break;
	case ARR_TYPE:
	    switch(bt.getId()){
	    case INT:
		t = new Type(Id.ARR_INT);
		System.out.println("Int array");
		break;
	    case CHAR:
		t = new Type(Id.ARR_CHAR);
		System.out.println("Char array");
		break;
	    default:
		error("Unknown type");
		return false;
	    }
	    break;
	default:
	    error("Unexpected Variable Declaration: " + varDec.getId());
	    return false;
	}
	IdentifierNode id = (IdentifierNode)dr.getChild(0);
	env.insert(id.getName(), t, id.getPosition());
	System.out.println("");
	return true;
    }
    
    static boolean checkBody(CompoundStatementNode body, Env env){
	boolean flag = true;
	List<Node> declarations = body.getDeclarations();
	List<Node> statements = body.getStatements();
	for(Node dec : declarations){
	    flag = checkVarDec(dec, env);
	}
	for(Node stm : statements){
	    flag = checkStatement(stm, env);
	}
	return flag;
    }
    static boolean checkStatement(Node stm, Env env){
	boolean flag = true;
	boolean flag1 = true;
	switch(stm.getId()){
	case SIMPLE_COMPOUND_STMNT:
	    return true;
	case STMNT:
	    System.out.println("STMNT");
	    return checkExpression(stm.getChild(0), env);
	case EMPTY_STMNT:
	    System.out.println("Empty Stm");
	    return true;
	case IF:
	    int n = stm.numberOfChildren();
	    flag1 = checkExpression(stm.getChild(0,n), env);
	    boolean flag2 = checkStatement(stm.getChild(1,n), env);
	    if(n==3)
		flag = checkStatement(stm.getChild(2,n), env);
	    return flag && flag1 && flag2;  
	case WHILE:
	    flag1 = checkExpression(stm.getChild(0,2), env);
	    flag = checkStatement(stm.getChild(1,2), env);
	    return flag && flag1;
	case RETURN:
	    return checkReturn(stm, env);
	default:
	    error("Invalid statement");
	    return false;
	}
    }
    
    static boolean checkReturn(Node r, Env env){
	boolean flag = true;
	int n = r.numberOfChildren();
	Type t = env.getResult();
	
	if(n==1)
	    flag = checkExpression(r.getChild(0), env);
	return flag;
    }
    
    static boolean checkExpression(Node exp, Env env){
	boolean flag = true;
	switch(exp.getId()){
	case BINARY:
	    System.out.println("Binary");
	    return checkBinary((BinaryNode)exp, env);
	case UNARY:
	    System.out.println("Unary");
	    return checkExpression(exp.getChild(0), env);
	case FCALL:
	    System.out.println("FCall");
	    return checkFCall((FCallNode)exp, env);
	case ASSIGN:
	    System.out.println("Assign");
	    //exp.print("");
	    flag = checkAssign(exp, env);
	    return flag;
	case CHAR_LITERAL:
	    System.out.println("Char");
	    ((LocalEnv)env).setLatestExp(new Type(Id.CHAR));
	    return true;
	case INTEGER_LITERAL:
	    ((LocalEnv)env).setLatestExp(new Type(Id.INT));
	    System.out.println("Int");
	    return true;
	case IDENT:
	    System.out.println("Ident");
	    String name = ((IdentifierNode)exp).getName();
	    Type identType = env.lookup(name);
	    System.out.println("Vafan " + identType);
	    ((LocalEnv)env).setLatestExp(identType);
	    if(identType == null){
		error("Variable " + name + " not defined.");
		return false;
	    }
	    return true;
	case ARRAY:
	    System.out.println("Array");
	    IdentifierNode id = (IdentifierNode)exp.getChild(0,2);
	    Type t = env.lookup(id.getName());
	    if(t.getId() == Id.ARR_CHAR){
		t = new Type(Id.CHAR);
	    }else{
		t = new Type(Id.INT);
	    }
	    ((LocalEnv)env).setLatestExp(t);
	    return true;
	default:
	    error("Invalid expression.");
	    return false;
	}
    }

    static boolean checkAssign(Node exp, Env env){
	boolean flag = true;
	Node var = exp.getChild(0,2);
	IdentifierNode var2;
	Type expType, t;
	if(var.getId() == Id.ARRAY){
	    var2 = (IdentifierNode)var.getChild(0,2);
	    if(!checkExpression(var.getChild(1,2), env) ||
	       !(((LocalEnv)env).getLatestExp().getId() == Id.INT)) {
		return false;
	    }
	    t = env.lookup(var2.getName());
	    if(t.getId() == Id.ARR_CHAR){
		t = new Type(Id.CHAR);
	    }else{
		t = new Type(Id.INT);
	    }
	} else {
	    var2 = (IdentifierNode) var;
	    t = env.lookup(var2.getName());
	}
	flag = checkExpression(exp.getChild(1,2), env);
	expType = ((LocalEnv)env).getLatestExp();
	if(t==null || expType == null){
	    error("Variable " + var2.getName() + " not defined.");
	    return false;
	}
	if((t.getId() != expType.getId() &&
	    !(t.getId() == Id.INT && expType.getId() == Id.CHAR)) ||
	   (t.getId() == expType.getId() && (t.getId() == Id.ARR_CHAR) ||
	    t.getId() == Id.ARR_INT)) {
	    error("Type missmatch, expected " + t.getId() + " at " + var2.getPosition() + " got " + expType.getId());
	    return false;
	}
	return flag;
    }
    static boolean checkFCall(FCallNode fc, Env env){
	boolean flag = true;
	IdentifierNode id = (IdentifierNode)fc.getIdent();
	Type t = env.lookup(id.getName());
	List<Node> args = fc.getArgs();
	for(Node exp : args){
	    flag = flag && checkExpression(exp, env);
	}
	return flag;
    }

    static boolean checkBinary(BinaryNode b, Env env){
	boolean flag = true;
	switch(b.getOp()){
	case ANDAND:
	case EQEQ:
	case NE:
	case LT:
	case GT:
	case LTEQ:
	case GTEQ:
	case PLUS:
	case MUL:
	case DIV:
	case MINUS:
	    
	    boolean left = checkExpression(b.getChild(0,2), env);
	    if ( left == false) return false;
	    Type l = ((LocalEnv)env).getLatestExp();
	    boolean right = checkExpression(b.getChild(1,2), env);
	    if ( right == false) return false;
	    Type r = ((LocalEnv)env).getLatestExp();
	    System.out.println("l " + l + " r " + r);
	    if((l.getId() != r.getId()) &&
	       (l.getId() == Id.INT &&
		r.getId() == Id.CHAR) || 
	       (l.getId() == Id.CHAR &&
		r.getId() == Id.INT) &&
	       r.getId() != Id.ARR_CHAR && r.getId() != Id.ARR_INT &&
	       l.getId() != Id.ARR_CHAR && l.getId() != Id.ARR_INT) {
		((LocalEnv)env).setLatestExp(new Type(Id.INT));
		System.out.println("Kolla michal, johan har gjort rätt");
		return true;
	    } else if((l.getId() == r.getId()) &&
		      (l.getId() == Id.INT ||
		       l.getId() == Id.CHAR)) {
		((LocalEnv)env).setLatestExp(new Type(l.getId()));
		//System.out.println("Kolla michal du har gjort fel");
		return true;
	    }
	    System.out.println("Kolla michal du har gjort fel");
	    return false;
	default:
	    error("Fail");
	    return false;
	}
    }

    static boolean checkIsInt(Node b){
	return true;
    }
}
